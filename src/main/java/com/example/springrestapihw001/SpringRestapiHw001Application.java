package com.example.springrestapihw001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Customer REST API", version = "2.0", description = "Welcome to spring restapi by hongmeng"))
public class SpringRestapiHw001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestapiHw001Application.class, args);
    }

}
