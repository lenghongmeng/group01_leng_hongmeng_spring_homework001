package com.example.springrestapihw001;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/api/vi/customers")
public class CustomerController {

    Map<String, Object> map;
    private List<Customer> customerList = new ArrayList<>();
    private CustomerResponse customerResponse;
    String message;
    int id = 0;

    public CustomerController() {
        id++;
        customerList.add(new Customer(id, "Hongmeng", "Male", 25,"Kandal"));
        id++;
        customerList.add(new Customer(id, "Kuma", "Male", 26,"Phnom Penh"));
        id++;
        customerList.add(new Customer(id, "Moka", "Female", 15,"Phnom Penh"));
    }

    // get all customer list
    @GetMapping("/list")
    public ResponseEntity<?> listCustomer() {

        message = !customerList.isEmpty() ? "Customer list has data" : "Customer list has no data";

        if(customerList.isEmpty()) {
            map = new HashMap<>();
            map.put("message", message);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
        else {
            customerResponse = new CustomerResponse(LocalDateTime.now(), 200, message, customerList);
            return new ResponseEntity<>(customerResponse, HttpStatus.OK);
        }
    }

    // insert to list
    @PostMapping("/store")
    public ResponseEntity<?> storeCustomer(@RequestBody CustomerBody c) {
        id++;
        Customer customer = new Customer(id, c.getName(), c.getGender(), c.getAge(), c.getAddress());
        customerList.add(customer);
        customerResponse = new CustomerResponse(LocalDateTime.now(), 201, "Customer has created succesfully", customer);
        return new ResponseEntity<>(customerResponse, HttpStatus.CREATED);
    }

    // show specific customer
    @GetMapping("/show/{id}")
    public ResponseEntity<?> showCustomer(@PathVariable int id) {

        Customer customer = null;

        for (Customer cus:customerList) {
            if(cus.getId() == id) {
                customer = cus;
                break;
            }
        }

        message = customer != null ? "Customer has found successfully" : "Customer not found";

        if(customer == null) {
            map = new HashMap<>();
            map.put("message", message);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
        else  {
            customerResponse = new CustomerResponse(LocalDateTime.now(), 200, message, customer);
            return new ResponseEntity<>(customerResponse, HttpStatus.FOUND);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<?> showCustomerByName(@RequestParam String name) {

        List<Customer> getCustomers = new ArrayList<>();

        for (Customer cus:customerList) {
            if(cus.getName().toLowerCase().startsWith(name.toLowerCase())) {
                getCustomers.add(cus);
            }
        }

        message = getCustomers.isEmpty() ? "Customer not found" : "Customer has found successfully";

        if(!getCustomers.isEmpty()) {
            customerResponse = new CustomerResponse(LocalDateTime.now(), 200, message, getCustomers);
            return new ResponseEntity<>(customerResponse, HttpStatus.OK);
        }
        else {
            map = new HashMap<>();
            map.put("message", message);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable int id) {

        Customer customer = null;
        String getId = "";

        for (Customer cus:customerList) {
            if(cus.getId() == id) {
                customer = cus;
                getId = String.valueOf(cus.getId());
                break;
            }
        }

        message = customer != null ? "Customer ID " + getId + " has deleted succesfully" : "Customer not found";

        if(customer != null) {
            customerList.remove(customer);
            customerResponse = new CustomerResponse(LocalDateTime.now(), 200, message, null);
            return new ResponseEntity<>(customerResponse, HttpStatus.OK);
        }
        else {
            map = new HashMap<>();
            map.put("message", message);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    // Update customer
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCustomer(@RequestBody CustomerBody customerBody, @PathVariable int id) {

        Customer customer = null;
        int indexCustomer = 0;

        for (Customer cus:customerList) {
            if(cus.getId() == id) {
                customer = cus;
                indexCustomer = customerList.indexOf(cus);
                break;
            }
        }

        message = customer != null ? "Customer has updated successfully" : "Customer not found";
        
        if(customer == null) {
            map = new HashMap<>();
            map.put("message", message);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
        else {
            customer = new Customer(id, customerBody.getName(), customerBody.getGender(), customerBody.getAge(), customerBody.getAddress());
            customerList.set(indexCustomer, customer);
            customerResponse = new CustomerResponse(LocalDateTime.now(), 200, message, customer);
            return new ResponseEntity<>(customerResponse, HttpStatus.OK);
        }
    }
}
